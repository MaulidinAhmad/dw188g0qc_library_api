const { User } = require("../models");

exports.getUser = async (req, res) => {
  try {
    const data = User.findAll();
    res.send({
      message: "Successfuly Fetch Data",
      data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      error: {
        message: "Server Error",
      },
    });
  }
};

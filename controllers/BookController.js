const { Book, Category, User } = require("../models");

exports.getBooks = async (req, res) => {
  try {
    const data = await Book.findAll({
      include: [
        {
          as: "category",
          model: Category,
          attributes: {
            exclude: ["createdAt", "updatedAt"],
          },
        },
        {
          as: "user",
          model: User,
          attributes: {
            exclude: ["createdAt", "password", "updatedAt"],
          },
        },
      ],
      attributes: {
        exclude: ["createdAt", "updatedAt"],
      },
    });
    res.send({
      message: "Data Successfully Fetched",
      data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      error: {
        message: "Server Error",
      },
    });
  }
};
exports.updateBook = async (req, res) => {
  try {
    const { id } = req.params;

    await Book.update(req.body, {
      where: {
        id,
      },
    });
    const data = await Book.findOne({
      where: {
        id,
      },
      include: [
        {
          as: "category",
          model: Category,
          attrbutes: {
            exclude: ["createdAt", "updatedAt"],
          },
        },
        {
          as: "user",
          model: User,
          attributes: {
            exclude: ["createdAt", "updatedAt", "password"],
          },
        },
      ],
      attributes: {
        exclude: ["createdAt", "updatedAt", "userId", "categoryId"],
      },
    });
    res.send({
      message: `Succesfuly Update data with id ${id}`,
      data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      error: {
        message: "Server Error",
      },
    });
  }
};
exports.getBook = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Book.findOne({
      where: {
        id,
      },
      include: [
        {
          as: "category",
          model: Category,
          attributes: {
            exclude: ["createdAt", "updatedAt"],
          },
        },
        {
          as: "user",
          model: User,
          attributes: {
            exclude: ["createdAt", "updatedAt", "password"],
          },
        },
      ],
      attributes: {
        exclude: ["createdAt", "updatedAt", "userId", "categoryId"],
      },
    });

    res.send({
      message: "Succesfuly Fetched data",
      data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      error: {
        message: "Server Error",
      },
    });
  }
};
exports.addBook = async (req, res) => {
  try {
    const data = await Book.create(req.body);
    res.send({
      message: "Successfully add data",
      data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      error: {
        message: "Server Error",
      },
    });
  }
};
exports.deleteBook = async (req, res) => {
  try {
    const { id } = req.params;
    await Book.destroy({
      where: {
        id,
      },
    });

    res.send({
      message: `Successfully delete data with id ${id}`,
      data: {
        id,
      },
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      error: {
        message: "Server Error",
      },
    });
  }
};

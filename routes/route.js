const router = require("express").Router();

const {
  getBooks,
  getBook,
  updateBook,
  deleteBook,
  addBook,
} = require("../controllers/BookController");

// Book Routes
router.get("/books", getBooks);
router.get("/book/:id", getBook);
router.post("/book", addBook);
router.patch("/book/:id", updateBook);
router.delete("/book/:id", deleteBook);

module.exports = router;

const express = require("express");
const app = express();

const port = 4000;

app.use(express.json());

// Route
const route = require("./routes/route");

app.use("/api/v1", route);

// Run Server
app.listen(port, () => {
  console.log(`Server Running On Port ${port}`);
});
